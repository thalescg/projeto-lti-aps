package com.lti.aps.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.lti.aps.models.Paciente;

@Repository
public interface PacienteRepository extends CrudRepository<Paciente, Long> {

}
