package com.lti.aps.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.lti.aps.models.Localidade;

@Repository
public interface LocalidadeRepository extends CrudRepository<Localidade, Long> {

}
