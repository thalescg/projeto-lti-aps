package com.lti.aps.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.lti.aps.models.Endereco;

@Repository
public interface EnderecoRepository extends CrudRepository<Endereco, Long> {

}
