package com.lti.aps.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.hibernate.validator.constraints.br.CPF;

@Entity
public class Paciente {
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(nullable = false)
	private String nome;
	
	@Column(nullable = false)
	@CPF
	private String cpf;
	
	@Column(nullable = false, name = "mae")
	private String nomeDaMãe;
	
	@OneToOne(mappedBy = "endereco")
	private Endereco endereço;

	@Column(nullable = false, name = "data")
	private Date dataDeNascimento;
	
	@Column(nullable = false)
	private boolean sexo;
	
	@Column(nullable = false)
	private boolean emEspera;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNomeDaMãe() {
		return nomeDaMãe;
	}

	public void setNomeDaMãe(String nomeDaMãe) {
		this.nomeDaMãe = nomeDaMãe;
	}

	public Endereco getEndereço() {
		return endereço;
	}

	public void setEndereço(Endereco endereço) {
		this.endereço = endereço;
	}

	public Date getDataDeNascimento() {
		return dataDeNascimento;
	}

	public void setDataDeNascimento(Date dataDeNascimento) {
		this.dataDeNascimento = dataDeNascimento;
	}

	public boolean isSexo() {
		return sexo;
	}

	public void setSexo(boolean sexo) {
		this.sexo = sexo;
	}

	public boolean isEmEspera() {
		return emEspera;
	}

	public void setEmEspera(boolean emEspera) {
		this.emEspera = emEspera;
	}
	
	
	
	
}
