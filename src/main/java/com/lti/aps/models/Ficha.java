package com.lti.aps.models;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Ficha {
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(nullable = false, name = "Data")
	private Date dataDoAtendimento;
	
	@Column(nullable = false)
	private String sintomas;
	
	@Column(nullable = false)
	private String doenca;
	
	@Column(nullable = false)
	private boolean hipertenso;
	
	@Column(nullable = false)
	private boolean alergico;
	
	@Column(nullable = false)
	private boolean diabetes;
	
	@Column(nullable = false)
	private String prescicao;
	
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "paciente")
	private Paciente paciente;
	
	@Column(nullable = false)
	private boolean observacoes;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private Localidade localidade;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getDataDoAtendimento() {
		return dataDoAtendimento;
	}
	public void setDataDoAtendimento(Date dataDoAtendimento) {
		this.dataDoAtendimento = dataDoAtendimento;
	}
	public String getSintomas() {
		return sintomas;
	}
	public void setSintomas(String sintomas) {
		this.sintomas = sintomas;
	}
	public String getDoenca() {
		return doenca;
	}
	public void setDoenca(String doenca) {
		this.doenca = doenca;
	}
	public boolean isHipertenso() {
		return hipertenso;
	}
	public void setHipertenso(boolean hipertenso) {
		this.hipertenso = hipertenso;
	}
	public boolean isAlergico() {
		return alergico;
	}
	public void setAlergico(boolean alergico) {
		this.alergico = alergico;
	}
	public boolean isDiabetes() {
		return diabetes;
	}
	public void setDiabetes(boolean diabetes) {
		this.diabetes = diabetes;
	}
	public String getPrescicao() {
		return prescicao;
	}
	public void setPrescicao(String prescicao) {
		this.prescicao = prescicao;
	}
	public Paciente getPaciente() {
		return paciente;
	}
	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
	public boolean isObservacoes() {
		return observacoes;
	}
	public void setObservacoes(boolean observacoes) {
		this.observacoes = observacoes;
	}
	public Localidade getLocalidade() {
		return localidade;
	}
	public void setLocalidade(Localidade localidade) {
		this.localidade = localidade;
	}
	
	

}
