package com.lti.aps.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lti.aps.models.Paciente;
import com.lti.aps.repository.PacienteRepository;

@Service
public class PacienteService {

	@Autowired
	private PacienteRepository repository;
	
	public Paciente createPaciente(Paciente paciente){
		
		return this.repository.save(paciente);
		
	}
	
	public Paciente updatePaciente(Paciente paciente) {
		
		return this.repository.save(paciente);
		
	}
	
	public void deletePaciente(Long id) {
		
		this.repository.deleteById(id);
		
	}
	
	public List<Paciente> readPaciente(){
		
		return (List<Paciente>) this.repository.findAll();
		
	}
	
	
	
}
