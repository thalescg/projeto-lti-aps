package com.lti.aps.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lti.aps.repository.EnderecoRepository;

@Service
public class EnderecoService {
	
	// TODO Ver se vai Necessitar do Endereço Service e Controller
	@Autowired
	private EnderecoRepository repository;

	public EnderecoRepository getRepository() {
		return repository;
	}

	public void setRepository(EnderecoRepository repository) {
		this.repository = repository;
	}
	
	
	
}
