package com.lti.aps.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lti.aps.models.Localidade;
import com.lti.aps.repository.LocalidadeRepository;

@Service
public class LocalidadeService {
	
	@Autowired
	private LocalidadeRepository repository;
	
	public Localidade createLocalidade(Localidade localidade) {
		
		return this.repository.save(localidade);
		
	}
	
	public Localidade updateLocalidade(Localidade localidade) {
		
		return this.repository.save(localidade);
		
	}
	
	public void deleteLocalidade(Long id) {
		
		this.repository.deleteById(id);
				
	}
	
	public List<Localidade> readLocalidade(){
		
		return (List<Localidade>) this.repository.findAll();
		
	}
	
	

}
