package com.lti.aps.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lti.aps.models.Ficha;
import com.lti.aps.repository.FichaRepository;

@Service
public class FichaService {

	@Autowired
	private FichaRepository repository;
	
	public Ficha createFicha(Ficha ficha) {
		
		return this.repository.save(ficha);
		
	}
	
	public Ficha updateFicha(Ficha ficha) {
		
		return this.repository.save(ficha);
		
	}
	
	public void deleteFicha(Long id) {
		
		this.repository.deleteById(id);
		
	}
	
	public List<Ficha> readFicha(){
		
		return (List<Ficha>) this.repository.findAll();
	
	}
	
}
