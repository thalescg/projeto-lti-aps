package com.lti.aps.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lti.aps.models.Ficha;
import com.lti.aps.services.FichaService;

@Controller
@RequestMapping("api/ficha")
public class FichaController {

	@Autowired
	public FichaService service;
	
	@GetMapping
	public ResponseEntity<List<Ficha>> getAll(){
		
		List<Ficha> body = service.readFicha();
		return new ResponseEntity<List<Ficha>>(body, HttpStatus.OK);
		
	}
	
	@PostMapping
	public ResponseEntity<Ficha> postFicha(Ficha ficha){
		
		Ficha body = service.createFicha(ficha);
		return new ResponseEntity<Ficha>(body, HttpStatus.OK);
		
	}
	
	@PutMapping
	public ResponseEntity<Ficha> putFicha(Ficha ficha){
		
		Ficha body = service.updateFicha(ficha);
		return new ResponseEntity<Ficha>(body, HttpStatus.OK);
		
	}
	
	//TODO Criar DeleteFicha
	
	
	
}
