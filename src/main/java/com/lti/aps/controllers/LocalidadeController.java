package com.lti.aps.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lti.aps.models.Localidade;
import com.lti.aps.services.LocalidadeService;

@Controller
@RequestMapping("api/localidade")
public class LocalidadeController {

	@Autowired
	public LocalidadeService service;
	
	@GetMapping
	public ResponseEntity<List<Localidade>> getAll(){
		
		List<Localidade> body = service.readLocalidade();
		return new ResponseEntity<List<Localidade>>(body, HttpStatus.OK);
		
	}
	
	@PostMapping
	public ResponseEntity<Localidade> postLocalidade(Localidade localidade){
		
		Localidade body = service.createLocalidade(localidade);
		return new ResponseEntity<Localidade>(body, HttpStatus.OK);		
		
	}
	
	@PutMapping
	public ResponseEntity<Localidade> putLocalidade(Localidade localidade){
		
		Localidade body = service.updateLocalidade(localidade);
		return new ResponseEntity<Localidade>(body, HttpStatus.OK);
		
	}
	
	
	
}
