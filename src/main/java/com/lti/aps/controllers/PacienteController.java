package com.lti.aps.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lti.aps.models.Paciente;
import com.lti.aps.services.PacienteService;

@Controller
@RequestMapping(value = "api/paciente")
public class PacienteController {
	
	@Autowired
	private PacienteService service;
	
	@GetMapping
	public ResponseEntity<List<Paciente>> getAll(){
		
		List<Paciente> body = service.readPaciente();
		return new ResponseEntity<List<Paciente>>(body, HttpStatus.OK);

	}

	@PostMapping
	public ResponseEntity<Paciente> postPaciente(@RequestBody Paciente paciente){
		
		Paciente body = service.createPaciente(paciente);
		return new ResponseEntity<Paciente>(body, HttpStatus.OK);
		
	}
	
	@PutMapping
	public ResponseEntity<Paciente> putPaciente(@RequestBody Paciente paciente){
	
		Paciente body = service.updatePaciente(paciente);
		return new ResponseEntity<Paciente>(body, HttpStatus.OK);
		
	}
	
//	@DeleteMapping sem parametro?
// TODO Criar DeletePaciente
		
}
